CREATE VIEW v_generated_dates AS
    SELECT date_january::date FROM generate_series(
            timestamp '2022-01-01',
            timestamp '2022-01-31',
            '1 day'::interval) AS date_january;